package nlmk.nikulinvs.tm.dao;

import nlmk.nikulinvs.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskDAO {

    private List<Task> tasks = new ArrayList<>();

    public Task create (final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

    public static void main(String[] args) {
        final TaskDAO taskDAO = new TaskDAO();
        taskDAO.create("DEMO");
        System.out.println(taskDAO.findAll());
    }
}
